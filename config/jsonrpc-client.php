<?php

use App\PageData\PageDataRPCClient;

return [
    // Имя клиента. Используется в качестве префикса к ID запросов
    'clientName'  => 'site_',

    // Соединение по умолчанию
    'default'     => 'PageDataRPC',

    // Список соединений
    'connections' => [
        // Наименование соединения
        'PageDataRPC' => [
            // URL-адрес JsonRpc-сервера
            'url'             => env('PAGE_DATA_RPC_URL', 'http://127.0.0.1:8080/api/v1/jsonrpc'),
            // Имя прокси-класса для данного соединения
            'clientClass'     => '\\App\\PageData\\PageDataRPCClient',
            // Генерация расширенного описания АПИ в виде классов-хелперов для входных и выходных параметров методов
            'extendedStubs'   => true,
            'middleware'      => [
            ],
            'namedParameters' => true,
        ],
    ],
];
