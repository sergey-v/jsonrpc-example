<?php

namespace App\PageData\DTO;

class PageDataDTO
{
    /**
     * Идентификтор данных.
     *
     * @var int
     */
    private int $id;

    /**
     * Уникальный идентификатор страницы.
     *
     * @var string
     */
    private string $pageUid;

    /**
     * Key-value хранилище данных для страницы.
     *
     * @var object
     */
    private object $data;

    /**
     * Дата создания данных.
     *
     * @var \DateTimeInterface
     */
    private \DateTimeInterface $createdAt;

    /**
     * PageDTO constructor.
     * @param int $id Идентификатор данных
     * @param string $pageUid Уникальный идентификатор страницы.
     * @param object $data Key-value хранилище данных для страницы.
     * @param \DateTimeInterface $createdAt Дата создания данных.
     */
    public function __construct(int $id, string $pageUid, object $data, \DateTimeInterface $createdAt)
    {
        $this->id = $id;
        $this->pageUid = $pageUid;
        $this->data = $data;
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPageUid(): string
    {
        return $this->pageUid;
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        return $this->data;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
