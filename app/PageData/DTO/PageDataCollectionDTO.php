<?php

namespace App\PageData\DTO;

class PageDataCollectionDTO implements \IteratorAggregate, \Countable
{
    /**
     * @var array|PageDataDTO[]
     */
    private array $items;
    private int $limit;
    private int $offset;
    private int $total;

    /**
     * PageDataCollectionDTO constructor.
     * @param PageDataDTO[]|array $items
     * @param int $limit
     * @param int $offset
     * @param int $total
     */
    public function __construct(array $items, int $limit, int $offset, int $total)
    {
        $this->items = $items;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->total = $total;
    }

    /**
     * @return \ArrayIterator|\Traversable|PageDataDTO[]
     */
    public function getIterator() {
        return new \ArrayIterator($this->items);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return \count($this->items);
    }

    /**
     * @return PageDataDTO[]|array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
