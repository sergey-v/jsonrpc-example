<?php

namespace App\PageData;

use App\PageData\DTO\PageDataDTO;
use App\PageData\Exceptions\InternalRpcException;
use App\PageData\Exceptions\InvalidRequestException;
use App\PageData\Exceptions\UnknownErrorException;
use Carbon\Carbon;
use Tochka\JsonRpcClient\Client;
use Tochka\JsonRpcClient\Exceptions\JsonRpcClientException;

/**
 * Сохранение и получение данных для страницы.
 *
 * @method static bool pageData_store(string $pageUid, object $data)
 * @method static array pageData_list(string $pageUid, int $limit = 10, int $offset = 0)
 */
class PageDataRPCClient extends Client
{
    /**
     * Сохранение данных для страницы.
     *
     * @param string $pageUid
     * @param object $data
     * @return bool
     *
     * @throws InvalidRequestException
     * @throws InternalRpcException
     * @throws UnknownErrorException
     */
    public function store(string $pageUid, object $data): bool
    {
        try {
            $response = self::pageData_store($pageUid, $data);

            return $response;
        } catch (JsonRpcClientException $e) {
            if ($e->getCode() == 6000) {
                throw new InvalidRequestException($e->getMessage(), $e->getCode(), $e);
            }
            throw new InternalRpcException($e->getMessage(), $e->getCode(), $e);
        } catch (\Throwable $e) {
            throw new UnknownErrorException($e);
        }
    }

    /**
     * Получнеие данных для страницы.
     *
     * @param string $pageUid
     * @param int $limit
     * @param int $offset
     * @return DTO\PageDataCollectionDTO
     * @throws \Throwable
     */
    public function list(string $pageUid, int $limit = 10, int $offset = 0): DTO\PageDataCollectionDTO
    {
        try {
            $response = self::pageData_list($pageUid, $limit, $offset);
            if (!isset($response->items, $response->total)) {
                throw new InternalRpcException("Invalid response");
            }

            $items = [];
            foreach ($response->items as $item) {
                $items[] = new PageDataDTO(
                    $item->id,
                    $item->page_uid,
                    $item->data,
                    \DateTime::createFromFormat(DATE_RFC3339, $item->created_at)
                );
            }

            return new DTO\PageDataCollectionDTO($items, $limit, $offset, $response->total);
        } catch (JsonRpcClientException $e) {
            if ($e->getCode() == 6000) {
                throw new InvalidRequestException($e->getMessage(), $e->getCode(), $e);
            }

            throw new InternalRpcException($e->getMessage(), $e->getCode(), $e);
        } catch (InternalRpcException $e) {
            throw $e;
        } catch (\Throwable $e) {
            throw new UnknownErrorException($e);
        }
    }
}
