<?php

namespace App\PageData\Exceptions;

class UnknownErrorException extends \Exception
{
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct("Unknown RPC Client Error", 0, $previous);
    }
}
