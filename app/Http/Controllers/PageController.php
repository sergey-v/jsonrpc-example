<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class PageController extends Controller
{
    public function view(string $slug, \Illuminate\Http\Request $request, \App\PageData\PageDataRPCClient $pageDataClient)
    {
        $page = $request->query('page', 1);
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $data = $pageDataClient->list($slug, $limit, $offset);
        $paginator = new LengthAwarePaginator($data, $data->getTotal(), $limit, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        return view('page', [
            'slug' => $slug,
            'data' => $paginator,
        ]);
    }

    public function post(string $slug, \Illuminate\Http\Request $request, \App\PageData\PageDataRPCClient $pageDataClient)
    {
        $request->validate([
            'name' => ['required', 'filled', 'min:2', 'max:255'],
            'email' => ['email', 'nullable'],
            'comment' => ['required', 'filled', 'min:2', 'max:255']
        ]);

        try {
            $data = $request->all();
            $response = $pageDataClient->store($slug, (object) $data);

            if ($response !== true) {
                return back()->with('warning', 'Unfortunately, we have a problem.');
            }

            return back()->with('success', 'Data created.');
        } catch (\Throwable $e) {
            Log::error("RPC: Can't save data", [
                'exception' => $e
            ]);
            return back()->with('warning', 'Unfortunately, we have a problem. Please try again later.');
        }
    }
}
