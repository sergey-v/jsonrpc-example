<?php

namespace App\Providers;

use App\PageData\PageDataRPCClient;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Tochka\JsonRpcClient\Client\HttpClient;
use Tochka\JsonRpcClient\ClientConfig;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PageDataRPCClient::class, function () {
            $services = config('jsonrpc-client.connections', []);
            $config = new ClientConfig('default', 'PageDataRPC', $services['PageDataRPC']);
            $client = new HttpClient();
            $queryPreparer = $this->app->get($config->queryPreparer);

            return new PageDataRPCClient($config, $queryPreparer, $client);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('datetime', function ($expression) {
            return "<?php echo ($expression)->format('m/d/Y H:i'); ?>";
        });
    }
}
