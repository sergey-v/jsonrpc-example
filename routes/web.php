<?php

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('page/{slug}', 'PageController@view')->name('viewPageData');
Route::post('page/{slug}', 'PageController@post')->name('postPageData');

Route::get('/', function (\App\PageData\PageDataRPCClient $pageDataClient) {
    //show latest 3 items only.
    $data = $pageDataClient->list('welcome', 3, 0);
    $paginator = new LengthAwarePaginator($data, 3, 3, 1);

    return view('welcome', [
        'data' => $paginator
    ]);
});
