FROM composer:1.10 as builder

WORKDIR /app
COPY ./composer.json /app/composer.json
COPY ./composer.lock /app/composer.lock
COPY ./database /app/database

RUN composer install --no-dev --optimize-autoloader --no-interaction --no-scripts --ignore-platform-reqs

FROM php:7.4-fpm-alpine as app


ENV APP_ENV=prod
ENV APP_DEBUG=false
ENV LOG_CHANNEL=stderr
ENV DB_HOST=postgres
ENV DB_PORT=5432
ENV DB_DATABASE=laravel
ENV DB_USERNAME=postgres
ENV DB_PASSWORD=postgres

#Install dephencies and extensions.
RUN set -ex \
    && apk update && apk add --no-cache ca-certificates postgresql-dev sqlite-dev zlib-dev icu-dev oniguruma-dev \
    && docker-php-ext-install pdo pdo_sqlite pdo_pgsql iconv intl mbstring \
    && docker-php-ext-enable pdo pdo_sqlite pdo_pgsql iconv intl mbstring \
    && rm -rf /var/cache/apk/* && rm -rf /tmp/*

WORKDIR /app
COPY ./ /app
COPY --from=builder /app/vendor /app/vendor
COPY --from=builder /usr/bin/composer /usr/bin/composer

RUN set -ex \
    && composer dump-autoload --no-dev --optimize --no-scripts \
    && composer run-script --no-dev post-autoload-dump
