@if (count($data) >= 1)
    <hr>
    <div class="list-group">
        @foreach ($data as $item)
            <div class="list-group-item">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">{{ $item->getPageUid() }}#{{ $item->getId() }}</h5>
                    <small class="text-muted">
                        @datetime($item->getCreatedAt())
                    </small>
                </div>
                <pre class="mb-1 text-left">@json($item->getData(), JSON_PRETTY_PRINT)</pre>
            </div>
        @endforeach
    </div>
    <div class="m-t-md mx-auto">
        {{ $data->withQueryString()->links() }}
    </div>
    <hr>
@else
    <hr>
    No data for page
    <hr>
@endif


