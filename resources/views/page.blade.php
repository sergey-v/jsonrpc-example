@extends('layout')

@section('title', 'Page#'.$slug)

@section('content')
    <h1>Page#{{ $slug }}</h1>
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
                Session::forget('success');
            @endphp
        </div>
    @endif
    @if(Session::has('warning'))
        <div class="alert alert-warning">
            {{ Session::get('warning') }}
            @php
                Session::forget('warning');
            @endphp
        </div>
    @endif
    <div class="card m-b-md">
        <div class="card-body">
            <h5 class="card-title">Add data to page#{{ $slug }}</h5>
            <form method="POST" action="{{ route('postPageData', ['slug' => $slug]) }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="form_name">Name *</label>
                    <input name="name" type="text" class="form-control" id="form_name">
                    @if ($errors->has('name'))
                        <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="form_email">Email address</label>
                    <input name="email" type="email" class="form-control" id="form_email">
                    @if ($errors->has('email'))
                        <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="form_comment">Your comment *</label>
                    <textarea name="comment" class="form-control" id="form_comment" rows="3"></textarea>
                    @if ($errors->has('comment'))
                        <div class="alert alert-danger">{{ $errors->first('comment') }}</div>
                    @endif
                </div>
                <div class="form-group form-check">
                    <input name="isSubscribed" type="checkbox" class="form-check-input" id="form_isSubscribed">
                    <label class="form-check-label" for="form_isSubscribed">Subscribe me to new data</label>
                    @if ($errors->has('isSubscribed'))
                        <div class="alert alert-danger">{{ $errors->first('isSubscribed') }}</div>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <div id="page_data__{{ $slug }}" class="m-b-md">
        @include('_pagedata_list', ['data' => $data])
    </div>
@endsection
